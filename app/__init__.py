from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_redis import Redis
from redis import Redis
from rq import Queue
import config, os


redis = Redis()
queue = Queue(connection=redis)
db = SQLAlchemy()
migrate = Migrate()


def init_app():
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object(config.DevelopmentConfig)
    db.init_app(app)
    db.app = app
    db.create_all()
    migrate.init_app(app, db, render_as_batch=True)
    
    # register blueprints
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)
    
    from .dashboard import dashboard as dashboard_blueprint
    app.register_blueprint(dashboard_blueprint, url_prefix='/dashboard')
    
    from .api import api as api_blueprint
    app.register_blueprint(api_blueprint, url_prefix='/api')

    import rq_dashboard
    app.register_blueprint(rq_dashboard.blueprint, url_prefix="/rq")
    
    return app