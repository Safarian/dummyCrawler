from . import db
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.orm import relationship

class AssociateBook(db.Model):
    __tablename__ = "associateBook"
    id = Column(Integer, primary_key=True)
    product_id = Column(Integer, ForeignKey('MP.id'))
    book_id = Column(Integer, ForeignKey('book.id'))
    created_on = Column(DateTime(timezone=True), server_default=db.func.now())
    updated_on = Column(DateTime(timezone=True), server_default=db.func.now(), server_onupdate=db.func.now())
    
class MainProduct(db.Model):
   __tablename__ = 'MP'
   __seachable__ = ['id']
   id = Column(Integer, primary_key=True)
   name = Column(String(255), nullable=False)
   description = Column(String(1200))
   img = Column(String(1200))
   slug = Column(String(500), nullable=False)
   current_price = Column(Integer,  nullable=False, default=0)
   special_price = Column(Integer,  nullable=False, default=0) 
   created_on = Column(DateTime(timezone=True), server_default=db.func.now())
   updated_on = Column(DateTime(timezone=True), server_default=db.func.now(), server_onupdate=db.func.now())
   child = relationship('AssociateBook', backref='MainProduct', lazy=True)
   
class Book(db.Model):
    __tablename__ = "book"
    __seachable__ = ['name','book_id']
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    book_id = Column(String(25), unique=True, nullable=False)
    current_price = Column(Integer,  nullable=False, default=0)
    special_price = Column(Integer,  nullable=False, default=0) 
    img = Column(String(555))
    created_on = Column(DateTime(timezone=True), server_default=db.func.now())
    updated_on = Column(DateTime(timezone=True), server_default=db.func.now(), server_onupdate=db.func.now())
    price = relationship('Price', backref='book', lazy=True)
    associate = relationship('AssociateBook', backref='book', lazy=True)

class Price(db.Model):
    __tablename__ = "price"
    id = Column(Integer, primary_key=True)
    book_id = Column(String(25), ForeignKey('book.book_id'), nullable=False)
    current_price = Column(Integer,  nullable=False)
    special_price = Column(Integer,  nullable=False)
    created_on = Column(DateTime(timezone=True), server_default=db.func.now())
    updated_on = Column(DateTime(timezone=True), server_default=db.func.now(), server_onupdate=db.func.now())