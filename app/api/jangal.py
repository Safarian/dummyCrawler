import requests
import json


r = requests.get('https://jangal.com/api/ui/uiproduct/list?page=1')
print("fetching:"+ r.url)
data = r.json()
with open('jangal.json', 'w') as file:
    json.dump(data, file, ensure_ascii=False)
    file.close()

with open('jangal.json', 'r') as file:
    data=file.read()
    data=json.loads(data)
    file.close()
with open('jangal.csv', 'w') as file:
    file.write("Title,Price,SpecialPrice,DiscountPercent,image\n")
    for x in range(0,len(data["Data"])):
       file.write(data["Data"][x]["Title"]+","+str(data["Data"][x]["Price"])+","+str(data["Data"][x]["SpecialPrice"])+","+str(data["Data"][x]["DiscountPrice"])+"\n")
    file.close()

totalpage = round(int(data["TotalCount"])/len(data["Data"]))+1
for page in range(2, totalpage):
    base_url='https://jangal.com/api/ui/uiproduct/list?page='
    url = base_url+str(page)
    print("fetching: "+url)
    r = requests.get(url)
    data = r.json()
    with open('jangal.json', 'w') as file:
        json.dump(data, file, ensure_ascii=False)
        file.close()
    with open('jangal.csv', 'a') as file:
        for x in range(0,len(data["Data"])):
            file.write(data["Data"][x]["Title"]+","+str(data["Data"][x]["Price"])+","+str(data["Data"][x]["SpecialPrice"])+","+str(data["Data"][x]["DiscountPrice"])+"\n")
        file.close()
