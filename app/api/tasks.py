#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests, json, urllib3, time, sys, re, os.path, lxml.html, pandas as pd
from urllib3.contrib.socks import SOCKSProxyManager
from .utils import url_checker, sub_cat, csvToBookTable, create_csv, productId_maker, get_products
from bs4 import BeautifulSoup
from flask import current_app


def zabanshop():
    url = 'http://zaban.shop'
    #check for valid url
    url_checker(url)
    # get the sub categories
    hrefs = sub_cat(url)
    proxy = SOCKSProxyManager('socks5://localhost:9150/')
    count = 1
    records = []
    try:
        for href in hrefs:
            url = href.attrib['href'] + '/?pageitems=300' + '&only_available=1'
            if re.search('\d*_\d*_\d*', url):
                print(f"[{count}/" + str(len(hrefs)) + "]")
                print(url)
                response = proxy.request('GET', url).data
                time.sleep(1)
                soup = BeautifulSoup(response, 'html.parser')
                items = soup.find_all('div', attrs={'class':'product-item'})
                print(f"============= items: {len(items)} ============")
                for item in items:
                    print('new item')
                    if item.find("a", {'class':'product-name'}) != None or item.find('span',{'class':{'productPrice'}}) != None:
                        name = item.find("a", {'class':'product-name'}).text.strip()
                        productId = productId_maker(item.find("a", {'class':'product-image'}).get('href').split('/')) 
                        price = item.find('span',{'class':{'productOldPrice'}}).text.split('تومان')[0] if item.find('span',{'class':{'productOldPrice'}}) !=None else item.find('span',{'class':{'productPrice'}}).text.split('تومان')[0]
                        specialprice= item.find('span', {'class':'productSpecialPrice'}).text.split('تومان')[0] if item.find('span', {'class':'productSpecialPrice'}) != None else 0
                        img = str(item.find('img', {'class':'img-responsive'}).get('src')) if str(item.find('img', {'class':'img-responsive'}).get('src')) != None else "#"
                        records.append((name, productId, price, specialprice, img))
                count +=1
            else:
                print(f"[{count}/{str(len(hrefs))}]")
                print(" LINK WAS NOT VALID")
                count +=1
    except Exception as e:
        print(e)
    finally:
        # create_csv(records, 'zabanshop')
        print('CSV file is ready.')
        print('================================')
        csvToBookTable('/home/mahdi/Projects/Uni/dummyCrawler/data/', 'zabanshop.csv', ',')
        print('New data are in DB.')


def jangal():
    try:
        r = requests.get('https://jangal.com/api/ui/uiproduct/list?page=1')
        print("fetching:"+ r.url)
        data = r.json()
        with open('jangal.json', 'w') as file:
            json.dump(data, file, ensure_ascii=False)

        with open('jangal.json', 'r') as file:
            data=file.read()
            data=json.loads(data)
            
        with open('jangal.csv', 'w') as file:
            file.write("Title,Price,SpecialPrice,DiscountPercent,image\n")
            for x in range(0,len(data["Data"])):
                file.write(data["Data"][x]["Title"]+","+str(data["Data"][x]["Id"])+"-jangal"+","+str(data["Data"][x]["Price"])+","+str(data["Data"][x]["SpecialPrice"])+","+str(data["Data"][x]["DiscountPrice"])+"\n")

        totalpage = round(int(data["TotalCount"])/len(data["Data"]))+1
        for page in range(2, totalpage):
            base_url='https://jangal.com/api/ui/uiproduct/list?page='
            url = base_url+str(page)
            print("fetching: "+url)
            r = requests.get(url)
            data = r.json()
            with open('jangal.json', 'w') as file:
                json.dump(data, file, ensure_ascii=False)
                
            with open('jangal.csv', 'a') as file:
                for x in range(0,len(data["Data"])):
                    file.write(data["Data"][x]["Title"]+","+str(data["Data"][x]["Price"])+","+str(data["Data"][x]["SpecialPrice"])+","+str(data["Data"][x]["DiscountPrice"])+"\n")
    except Exception as e:
        return f"Error: {e}"