from flask import request, current_app, abort, jsonify, make_response, flash, render_template, redirect, url_for
from . import api
from .. import db, queue, redis
from ..models import MainProduct, Book, Price
from .tasks import zabanshop, jangal
import os
from .. import init_app, config

app = init_app()
app.app_context()


@api.route("/livesearch",methods=["POST","PUT"])
def livesearch():
    if request.method == "POST":
        name = request.form.get('name')
        books = Book.query.filter(Book.name.like(f"%{name}%")).limit(10)
        data = []
        for book in books:
            data.append({'name': book.name, 'PID': book.book_id, 'img': book.img})
        return jsonify(data)
    elif request.method == "PUT":
        try:
            searchbox = request.form.get("text")
            books = MainProduct.query.filter(MainProduct.name.like(f"%{searchbox}%")).limit(10)
            data = []
            for book in books:
                data.append({'name': book.name, 'PID': book.id, 'img': book.img, 'slug': book.slug})
            return jsonify(data)
        except Exception as e:
            return e

@api.route('/queue/info', methods=['GET','UPDATE'])
def rqInfo():
    stream = os.popen('rq info -W')
    output = stream.read()
    return jsonify({'message':output})



@api.route('/crawler/run', methods=['POST', 'GET', 'PUT', 'DELETE'])
def runCrawler():
    if request.method == 'GET':
        job = queue.enqueue(zabanshop, job_timeout=3600 )
        q_len = len(queue)
        message = f"Task queued at {job.enqueued_at.strftime('%m/%d/%Y, %H:%M:%S')}. Nmber of queues {q_len}. job id: {job.id}. Function: zabanshop"
        return redirect(url_for('main.index'))

