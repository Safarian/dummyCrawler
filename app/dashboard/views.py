from flask import render_template, flash, redirect, url_for, request, current_app, abort, jsonify, make_response
from sqlalchemy import update

from .. import db
from . import dashboard
from .utils import resolve_conflict, allowed_file, sluger, csvChecker
from .forms import uploadProductForm, addProductForm
from ..models import MainProduct, Book, Price, AssociateBook

from werkzeug.utils import secure_filename

import os
from os import listdir
from os.path import isfile, join
from datetime import datetime

from time import sleep
import csv

@dashboard.route('/uploadproduct', methods=[ "PUT"])
def uploadProduct():
    uploadFolder = current_app.config['UPLOAD_FOLDER']
    form = uploadProductForm()
    if request.method == "PUT":
        sleep(1)
        try:
            File = form.csvfile.data
            delim = form.delimiter.data
            if File and allowed_file(File.filename):
                csvfilename = resolve_conflict(uploadFolder, File.filename)
                csvfilename = secure_filename(csvfilename)
                File.save(os.path.join(uploadFolder, csvfilename))
                if csvChecker(os.path.join(uploadFolder, csvfilename),delim) == True:
                # add to db
                    with open(os.path.join(uploadFolder, csvfilename)) as csv_file:
                        csv_reader = csv.reader(csv_file, delimiter=delim)
                        for row in csv_reader:
                            existsBook = Book.query.filter(Book.book_id == row[1]).first()
                            if not existsBook:
                                print(f"{row[1]} dosent exist")
                                book = Book(name=row[0], book_id=row[1],current_price=row[2], special_price=row[3], img=row[4])
                                price = Price(book_id=row[1],current_price=row[2],special_price=row[3])
                                db.session.add(book)
                                db.session.add(price)
                                db.session.commit()
                            elif existsBook.current_price != int(row[2]) or existsBook.special_price != int(row[3]):
                                print(row[1])
                                existsBook.current_price = row[2]
                                existsBook.special_price = row[3]
                                existsBook.updated_on = db.func.now()
                                price = Price(book_id=row[1],current_price=row[2],special_price=row[3])
                                db.session.add(price)
                                db.session.commit()
                                
                                # remove the file
                                os.remove(os.path.join(uploadFolder, csvfilename))
                    return f"db is uptodate now."
                else:
                    return "columns format is not correct, why?"
                            
            else:
                abort(415, description="only csv file is allowed.")
            
        except Exception as e:
            print(e)
            return f"{e}"
        # isthisFile.save(+isthisFile.filename)
    return "nope not recived"


@dashboard.route('/')
@dashboard.route('/show-product')
def showProduct():
    data = MainProduct.query.all()
    return render_template('show-product.html', title="لیست محصولات", data=data)

@dashboard.route("/add-product", methods=['GET','POST'])
def addProduct():
    form = addProductForm()
    upload_form = uploadProductForm()
    if request.method == 'POST' and form.validate_on_submit():
        name = form.name.data
        slug = sluger(form.slug.data)
        description = form.description.data
        relatedProdcuts = request.form["IRP"]
        relatedProdcuts = relatedProdcuts.split(',')
        currentPrice = form.price.data
        specialPrice = form.specialPrice.data
        img = form.img.data
        form.name.data = ''
        form.slug.data = ''
        form.description.data = ''
        form.img.data = ''
        form.price.data = ''
        form.specialPrice.data = ''
        product = MainProduct(name=name, description=description, img=img, slug=slug, current_price=currentPrice, special_price=specialPrice)
        db.session.add(product)
        db.session.commit()
        currentProdcut = MainProduct.query.filter(MainProduct.slug==slug).first()
        
        # Add new relations
        for relatedProdcut in relatedProdcuts:
            if relatedProdcut != "":
                db.session.add(AssociateBook(product_id=currentProdcut.id,book_id=relatedProdcut))
                db.session.commit()
            
        flash("successfully added to db", 'success')
        return redirect(url_for('dashboard.addProduct'))
        
    return render_template('add-product.html', 
                           title="افزودن محصول اصلی", 
                           form=form, 
                           upload_form=upload_form)
    
@dashboard.route("/edit-product/<int:id>", methods= ['GET','POST'])
def editProduct(id):
    product = MainProduct.query.filter(MainProduct.id == id).first_or_404()
    commons = db.session.query(Book, AssociateBook).join(AssociateBook, (Book.book_id == AssociateBook.book_id)&(AssociateBook.product_id == id)).all()
    form = addProductForm()
    if request.method == 'GET':
        form.name.data = ''
        form.slug.data = ''
        form.description.data = ''
        form.img.data = ''
        # populate form fields
        form.name.data = product.name
        form.slug.data = product.slug
        form.description.data = product.description
        form.img.data = product.img   
        form.price.data = product.current_price
        form.specialPrice.data = product.special_price
        return render_template('edit-product.html', product=product, form=form, title=product.name, commons=commons)

    if request.method == 'POST' and form.validate_on_submit():
        # Update product information 
        product.name = form.name.data
        product.slug = sluger(form.slug.data)
        product.description = form.description.data
        currentRelations = request.form["IRP"]
        currentRelations = currentRelations.split(',')
        product.current_price = form.price.data
        product.special_price = form.specialPrice.data
        product.img = form.img.data
        form.name.data = ''
        form.slug.data = ''
        form.description.data = ''
        form.img.data = ''
        db.session.commit()
        oldRelations = []
        product = AssociateBook.query.filter(AssociateBook.product_id == id).all()
        for p in product:
            oldRelations.append(p.book_id)
        
        # Add new relations
        for currentRelation in currentRelations:
            if currentRelation not in oldRelations and currentRelation != "":
                newRelation = AssociateBook(product_id=id, book_id=currentRelation)
                db.session.add(newRelation)
                db.session.commit()
                
        # Remove relation.
        for oldRelation in oldRelations:
            if oldRelation not in currentRelations:
                removeRelation = AssociateBook.query.filter(AssociateBook.product_id==id, AssociateBook.book_id==oldRelation).first_or_404()
                db.session.delete(removeRelation)
                db.session.commit()        

        flash('Update successfully', 'success')   
        return redirect(url_for('dashboard.editProduct',id=id))
    

    
@dashboard.route("/delete-product/<id>", methods=['POST'])
def deleteProduct(id):
    product = MainProduct.query.filter(MainProduct.id == id).first()
    relateds = AssociateBook.query.filter(AssociateBook.product_id == id).all()
    for related in relateds:
        db.session.delete(related)
    db.session.delete(product)
    db.session.commit()
    flash('successfully deleted from db', 'success')
    return redirect(url_for("dashboard.showProduct"))
        
   

@dashboard.route('/show-list-csv')
def showListCsv():
    onlyfiles = [f for f in listdir(current_app.config['UPLOAD_FOLDER']) if isfile(join(current_app.config['UPLOAD_FOLDER'], f))]
    return render_template('show-list-csv.html', files=onlyfiles)

