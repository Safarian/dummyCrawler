from flask_wtf import FlaskForm

from wtforms import StringField, SubmitField, FileField, IntegerField, validators as v
from wtforms.validators import DataRequired, Length
from wtforms.widgets import TextArea

# add product form
class addProductForm(FlaskForm):
    name = StringField("نام محصول", validators=[DataRequired(message="فیلد نام باید خالی باشد")])
    slug = StringField('اسلاگ محصول')
    img = StringField('عکس محصول')
    price = IntegerField('قیمت اصلی')
    specialPrice = IntegerField('قیمت ويژه')
    description = StringField('توضیحات محصول', widget=TextArea(),)
    submit = SubmitField('ثبت')
    
class uploadProductForm(FlaskForm):
    csvfile = FileField(label='اپلود فایل', 
                        description='درون ریزی لیست محصولات به صورت یکجا', 
                        validators=[DataRequired(message="باید یک فایلی را اپلود کنید")])
    delimiter = StringField(label='نوع جدا کننده', 
                            description='کارکتری که عناصر موجود در فایل csv را از یکدیگر جدا میکند.', 
                            render_kw={'placeholder':'مثلا ;'},
                            validators=[DataRequired(message="جدا کننده را مشخص کنید")])
    csrf = True
    
    
    