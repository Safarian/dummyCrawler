import pandas as pd
from flask import current_app as app
from time import asctime
import os

# replace "_" insted of empty space.
def sluger(slug):
    slug = slug.split(' ')
    slug = "_".join(slug)
    return slug

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']
           
# No duplicate no renewal
def resolve_conflict(target_folder, basename):
        name, ext = os.path.splitext(basename)
        count = 0
        while True:
            count = count + 1
            newname = f'{name}_{count}{ext}'
            if not os.path.exists(os.path.join(target_folder, newname)):
                return newname
    
def csvChecker(filename, delimiter):
    if pd.read_csv(filename, delimiter=delimiter).shape[1] == 5:
        return True
    else:
        return False
    