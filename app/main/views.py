from flask import render_template, redirect,\
    url_for, abort, flash, current_app,\
    request, make_response
    
from . import main
from .. import db
from ..models import Book, Price, MainProduct, AssociateBook
from time import strftime
from rq.command import send_stop_job_command
from sqlalchemy import desc
import datetime


@main.route('/')
def index():
    books = MainProduct.query.order_by(desc(MainProduct.updated_on)).limit(10)
    return render_template('index.html', title="صفحه اصلی", books=books)

@main.route('/book/<id>', methods=['GET'])
def ShowBook(id=None):    
    id = id
    book = db.session.query(MainProduct).filter(MainProduct.slug == id).first_or_404(description='There is no data with this {} information.'.format(id))
    relateds = db.session.query(Book, AssociateBook).join(AssociateBook, (Book.book_id == AssociateBook.book_id)&(AssociateBook.product_id == book.id)).all()
    # TODO: fack data, remove later
    labels = [
        'JAN', 'FEB', 'MAR', 'APR',
        'MAY', 'JUN', 'JUL', 'AUG',
        'SEP', 'OCT', 'NOV', 'DEC']
    values = [
        967.67, 1190.89, 1079.75, 1349.19,
        2328.91, 2504.28, 2873.83, 4764.87]
    return render_template('product.html', title=book.name, book=book, relateds=relateds, labels=labels, values=values, max=20000)


# handle 404 errors
@main.errorhandler(404)
def not_found(error):
    return render_template('error.html', title="404, Page not found"), 404

@main.route('/subproduct/<id>', methods=['GET'])
def ShowSubProduct(id):
    subproduct = db.session.query(Book).filter(Book.book_id == id).first_or_404()
    return render_template('subproduct.html', title=subproduct.name , product=subproduct)


    
    