#!/usr/bin/env python
# -*- coding: utf-8 -*-
import urllib3
import requests
import lxml.html
import time
import sys 
import csv
from urllib3.contrib.socks import SOCKSProxyManager
import re
from bs4 import BeautifulSoup
from .. import db
from ..models import Book, Price
from sqlalchemy import update

def url_checker(url):
    url_split = url.split('/')
    args = ['http:', 'https:']
    if any(url_split[0] in i for i in args):
        return True
        
def sub_cat(url:str):
    result = url_checker(url)
    if result == True:
        print('executed successfuly')
    else:
        raise ValueError('please enter a valid url like "http://example.zz or https://example.zz"')
    try:
        print("============ Try to connect ============")
        http = urllib3.PoolManager()
        html = http.request('GET', url) 
        if html.status == 200:
            print('connected successfully')
            time.sleep(1)
            print("Getting categories links")
            time.sleep(1)
            root = lxml.html.fromstring(html.data)
            hrefs = root.xpath("//li[contains(@class, 'has-sub-category')]/ul/li/a")
            time.sleep(1)
            print(f"{len(hrefs)} categories stored")
            print("============== Next step ==============")
            return hrefs
        else:
            print(f'The connection aborted because of this http error: {html.status}')
    except Exception as e:
        print(e)    
        
def get_products(links):
    proxy = SOCKSProxyManager('socks5h://localhost:9150/')
    for href in links:
        url = href.attrib['href'] + '/?pageitems=120' + '&only_available=1'
        if re.search('\d*_\d*_\d*', url):
            print(f"[{count}/" + str(len_hrefs) + "]")
            response = proxy.request('GET', url).data
            time.sleep(1)
            soup = BeautifulSoup(response, 'html.parser')
            items = soup.find_all('div', attrs={'class':'product-item'})
            for item in items:
                if item.find("a", attrs={'class':'product-ame'}) == None:
                    pass
                elif item.find('span', {'class':{'productPrice'}}) == None:
                    name = item.find("a", {'class':'product-name'}).text.strip()
                    productId = str(item.find("a", {'class':'product-name'}).get('src')).slpit('/')
                    price = "None"
                    specialprice= item.find('span', {'class':'productSpecialPrice'}).text
                    oldprice = item.find('span', {'class':'productOldPrice'}).text
                    img = str(item.find('img', {'class':'img-responsive'}).get('src'))
                else:
                    name = item.find("a", {'class':'product-name'}).text.strip()
                    productId = str(item.find("a", {'class':'product-name'}).get('src')).slpit('/')
                    price = item.find('span',{'class':{'productPrice'}}).text
                    specialprice= "None"
                    oldprice = "None"
                    img = str(item.find('img', {'class':'img-responsive'}).get('src'))
                productId = productId[-2].split('-')
                productId = 'zabanshop-'+productId[-1]
            records.append((name, productId, price, specialprice, oldprice, img))
                       
def productId_maker(href):
    href = href[-2]
    href = href.split('-')
    href = "zabanshop-"+href[-1]
    return href 

def create_csv(data,name='default'):
    try:
        print('========= Creating CSV ==========')
        if os.path.isdir('data') == False:
            os.mkdir('data')
        if os.path.isfile(f"data/{name}.csv"):
            os.remove(f"data/{name}.csv")
        df = pd.DataFrame(data)
        df.to_csv(f"data/{name}.csv", index=False, encoding='utf-8', sep=',')
        return "CSV file is ready"
    except Exception as e:
        print(e)

# 
import os
def csvToBookTable(uploadFolder, csvfilename):
    with open(os.path.join(uploadFolder, csvfilename)) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        for row in csv_reader:
            existsBook = Book.query.filter(Book.book_id == row[1]).first()
            if not existsBook:
                print(f"{row[1]} dosent exist")
                book = Book(name=row[0], book_id=row[1],current_price=row[2], special_price=row[3], img=row[4])
                price = Price(book_id=row[1],current_price=row[2],special_price=row[3])
                db.session.add(book)
                db.session.add(price)
                db.session.commit()
            elif existsBook.current_price != int(row[2]) or existsBook.special_price != int(row[3]):
                print(row[1])
                existsBook.current_price = row[2]
                existsBook.special_price = row[3]
                existsBook.updated_on = db.func.now()
                price = Price(book_id=row[1],current_price=row[2],special_price=row[3])
                db.session.add(price)
                db.session.commit()
            
                