from flask import Blueprint

main = Blueprint('main', __name__, template_folder='templates', static_folder='statics')

from . import views
from ..models import *