import os

app_dir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'A SECRET KEY'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///data.db'
    SQLALCHEMY_BINDS = ""
    UPLOAD_FOLDER = app_dir+'/data/uploads'
    DATA_FOLDER = app_dir+'/data'
    ALLOWED_EXTENSIONS = set(['csv'])
    REDIS_URL = 'redis://localhost:6379/0'

class DevelopmentConfig(Config):
    DEBUG = True
    ENV = 'development'
    DEVELOPMENT = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = True


class ProductionConfig(Config):
    DEBUG = False
